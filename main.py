from sklearn import tree
#import pandas as pd
import graphviz
import pydot
#pd.read_sql( )

#data
Y = ['send letter' , 'not' , 'not', 'send letter' , 'not'] # LABEL
# [IsPrimary, Suitability Changed , Isunique Combination, Smooth skin]
X = [[True, True, True], [True, False, False], [False,True, False], [False, False, True], [False, False, False]]

clf = tree.DecisionTreeClassifier()
clf.fit(X, Y)


tree.export_graphviz(clf, out_file='tree.dot')
prediction = clf.predict([[True, False, True]])
from subprocess import check_call
check_call(['dot','-Tpng','tree.dot', '-o', 'OutputFile.png'])
print(prediction)


